import numpy as np
# from neupy import algorithms


zero = np.array([
    1, 1, 1, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 1, 1, 1,
])

one = np.array([
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
])

two = np.array([
    1, 1, 1, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    1, 1, 1, 1,
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 1, 1, 1,
])

three = np.array([
    1, 1, 1, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 1, 1, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    1, 1, 1, 1,
])

four = np.array([
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 1, 1, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
])

five = np.array([
    1, 1, 1, 1,
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 1, 1, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    1, 1, 1, 1,
])

six = np.array([
    1, 1, 1, 1,
    1, 0, 0, 0,
    1, 0, 0, 0,
    1, 1, 1, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 1, 1, 1,
])

seven = np.array([
    1, 1, 1, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
])

eight = np.array([
    1, 1, 1, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 1, 1, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 1, 1, 1,
])

nine = np.array([
    1, 1, 1, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    1, 1, 1, 1,
    0, 0, 0, 1,
    0, 0, 0, 1,
    1, 1, 1, 1,
])


def draw_bin_image(image_matrix):
    print("_______________")
    for row in image_matrix.tolist():
        print('|' + ' '.join(' +'[val] for val in row) + ' |')


def getWightsArray(array):
    array = ZerosToMinus(array, 'forward')
    matrix = []
    for i in range(0, len(array)):
        for j in range(0, len(array)):
            if i == j:
                matrix.append(0)
            else:
                matrix.append(array[i] * array[j])
    return matrix


def ZerosToMinus(array, action):
    res = []
    if action == 'forward':
        for i in range(0, len(array)):
            if array[i] == 0:
                res.append(-1)
            else:
                res.append(1)
    elif action == 'back':
        for i in range(0, len(array)):
            if array[i] < 5:
                res.append(0)
            else:
                res.append(1)
    return res


def predict(array, half):
    predict = []
    counter = 0
    j = 0
    for i in range(0, len(array)):
        counter += array[i] * half[j]
        if j == len(half)-1:
            predict.append(counter)
            j = -1
            counter = 0
        j += 1
    return predict


def add(x, y):
    return list(map(lambda a, b: a + b, x, y))


draw_bin_image(zero.reshape((7, 4)))
draw_bin_image(one.reshape((7, 4)))
draw_bin_image(two.reshape((7, 4)))
draw_bin_image(three.reshape((7, 4)))
draw_bin_image(four.reshape((7, 4)))
draw_bin_image(five.reshape((7, 4)))
draw_bin_image(six.reshape((7, 4)))
draw_bin_image(seven.reshape((7, 4)))
draw_bin_image(eight.reshape((7, 4)))
draw_bin_image(nine.reshape((7, 4)))

zero = getWightsArray(zero)
one = getWightsArray(one)
two = getWightsArray(two)
three = getWightsArray(three)
four = getWightsArray(four)
five = getWightsArray(five)
six = getWightsArray(six)
seven = getWightsArray(seven)
eight = getWightsArray(eight)
nine = getWightsArray(nine)

W = zero
W = add(W, one)
W = add(W, two)
W = add(W, three)
W = add(W, four)

W = add(W, five)
W = add(W, six)
W = add(W, seven)
W = add(W, eight)
W = add(W, nine)


half = np.array([
    0, 1, 1, 0,
    0, 0, 0, 1,
    1, 0, 0, 1,
    1, 0, 0, 1,
    0, 0, 0, 0,
    0, 0, 0, 1,
    0, 0, 0, 1,
])
# W = np.array([
#     0, -3, 3, 1,
#     -3, 0, -3, -1,
#     3, -3, 0, 1,
#     1, -1, 1, 0,
# ])
# half = np.array([
#     0, 0, 0, 1,
# ])
draw_bin_image(half.reshape((7, 4)))


half = ZerosToMinus(half, 'forward')
print(predict(W, half))
result = np.asarray(ZerosToMinus(predict(W, half), 'back'))
result = np.asarray(ZerosToMinus(predict(W, result), 'back'))
result = np.asarray(ZerosToMinus(predict(W, result), 'back'))

draw_bin_image(result.reshape((7, 4)))

half = ZerosToMinus(result, 'forward')
